

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class fechaTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class fechaTest
{
    fecha f;
    /**
     * Default constructor for test class fechaTest
     */
    public fechaTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
        System.out.println(f.dimeFecha());
    }
    
    @Test
    public void prueba1()
    {
        f = new fecha(24,01,2019);
        assertEquals("Verdadero", f.dimeFecha());
    }
    
    @Test
    public void prueba2()
    {
        f = new fecha(24,-6,2019);
        assertEquals("Falso", f.dimeFecha());
    }
    
    @Test
    public void prueba3()
    {
        f = new fecha(24,13,2019);
        assertEquals("Falso", f.dimeFecha());
    }
}



















/**
 * Write a description of class fecha here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class fecha
{
    // instance variables - replace the example below with your own
    int a;
    int b;
    int c;

    /**
     * Constructor for objects of class fecha
     */
    public fecha()
    {
        // initialise instance variables
        a = 1;
        b = 1;
        c = 1;
    }
    
    public fecha(int x, int y, int z)
    {
        a=x;
        b=y;
        c=z;
    }

    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public String dimeFecha()
    {
        // put your code here
        if ((a<=31) && (a>=1) && (b<=12) && (b>=1) && (c==2019)) 
            return "Verdadero";
        else
            return "Falso";
    }
    
    public int getA(){
        return a;
    }
    
    public int getB(){
        return b;
    }
    
    public int getC(){
        return c;
    }
    
    public void setA(int num){
        a=num;
    }

    public void setB(int num){
        b=num;
    }

    public void setC(int num){
        c=num;
    }
}



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class trianguloTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class trianguloTest
{
    triangulo t;
    
    /**
     * Default constructor for test class trianguloTest
     */
    public trianguloTest()
    {
        
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
        System.out.println(t.dimeTipo());
    }
    
    @Test
    public void prueba1()
    {
        t = new triangulo(3,3,3);
        assertEquals("Equilatero", t.dimeTipo());
    }
    
    @Test
    public void prueba2()
    {
        t = new triangulo(5,3,3);
        assertEquals("Isosceles", t.dimeTipo());
    }
    
    @Test
    public void prueba3()
    {
        t = new triangulo(3,3,5);
        assertEquals("Isosceles", t.dimeTipo());
    }
    
    @Test
    public void prueba4()
    {
        t = new triangulo(3,5,3);
        assertEquals("Isosceles", t.dimeTipo());
    }
    
    @Test
    public void prueba5()
    {
        t = new triangulo(1,2,3);
        assertEquals("Escaleno", t.dimeTipo());
    }
    
    @Test
    public void prueba6()
    {
        t = new triangulo(3,0,3);
        assertEquals("No se puede tener un lado 0", t.dimeTipo());
    }
    
    @Test
    public void prueba7()
    {
        t = new triangulo(3,-1,3);
        assertEquals("No se puede tener un lado negativo", t.dimeTipo());
    }
    
    @Test
    public void prueba8()
    {
        t = new triangulo(3,Integer.MAX_VALUE+1,3);
        assertEquals("Es muy grande", t.dimeTipo());
    }
    
    @Test
    public void prueba9()
    {
        t = new triangulo(3,' ',3);
        assertEquals("No se permiten cosas raras", t.dimeTipo());
    }  
 }

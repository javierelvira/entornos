
/**
 * Write a description of class triangulo here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class triangulo
{
    // instance variables - replace the example below with your own
    int a;
    int b;
    int c;

    /**
     * Constructor for objects of class triangulo
     */
    public triangulo()
    {
        // initialise instance variables
        a = 1;
        b = 1;
        c = 1;
    }
    
    public triangulo(int x, int y, int z)
    {
        a=x;
        b=y;
        c=z;
    }

    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public String dimeTipo() 
    {
        // put your code here
        if (b==(' '))
            return "No se permiten cosas raras";
        if (b==(Integer.MAX_VALUE+1))
            return "Es muy grande";
        if (b==0)
            return "No se puede tener un lado 0";
        if (b<=0)
            return "No se puede tener un lado negativo";
        if ((a==b) && (b==c))
            return "Equilatero";
        if ((a==b) && (b!=c))
            return "Isosceles";
        if ((a==c) && (a!=b))
            return "Isosceles";
        if ((a!=b) && (b==c))
            return "Isosceles";
        else 
            return "Escaleno";
    }
    
    public int getA(){
        return a;
    }
    
    public int getB(){
        return b;
    }
    
    public int getC(){
        return c;
    }
    
    public void setA(int num){
        a=num;
    }

    public void setB(int num){
        b=num;
    }

    public void setC(int num){
        c=num;
    }
}







